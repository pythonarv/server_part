import pickle
from threading import Thread
import socket
from io import BytesIO
from sys import getsizeof
from PIL import Image


class ClientThread(Thread):

    MAIN_POOL = 0
    SKIPED_POOL = 1
    HISTORY_POOL = 2

    def __init__(self, server, conn):
        Thread.__init__(self)
        self.setDaemon(True)
        self.server = server
        self.conn = conn
        self.username = None
        self.current_pool = self.MAIN_POOL
        self.last_pool_is_skip = False
        self.authenticated = False
        self.max_auth_attempts = server.getConfig('AuthAttempt')
        self.auth_attempts = 0
        self.handlers = {
            'auth': self.auth,
            'tagImage': self.imageTag,
            'nextImage': self.sendNextImage,
            'prevImage': self.sendPrevImage
        }
        self.currentImage = None
        self.historySize = self.server.getConfig('historySize')
        self.history = []
        self.currentHistoryIndex = -1

    def run(self):
        while True:
            try:
                bufsize = self.conn.recv(16)
                if bufsize:
                    bufsize = int(bufsize)
                    data = self.conn.recv(bufsize)
                    if data:
                        key, data = self.deserialize_data(data)
                        if key in self.handlers and (self.authenticated or key == 'auth'):
                            self.handlers[key](data)
                else:
                    break
            except socket.error as err:
                break

            if not self.authenticated:
                self.auth_attempts += 1
                if self.auth_attempts > self.max_auth_attempts:
                    break
        self.onDisconnect()
        self.conn.close()

    def emit(self, key, data):
        data = (key, data)
        data = pickle.dumps(data)
        bufsize = '%16s' %len(data)

        self.conn.send(bufsize.encode())
        self.conn.send(data)

    def auth(self, data):
        self.authenticated = self.server.auth(data['username'], data['password'])
        response = {'success': False}
        if self.authenticated:
            self.username = data['username']
            response['success'] = True
            self.currentImage, image = self.nextImage()
            response['init_data'] = {
                'classes': self.server.getClasses(),
                'image': image
            }
            print('user %s authenticated' % data['username'])


        self.emit('auth_result', response)

    def prevImage(self):
        image_name = None
        image = None
        if len(self.history) > 0:
            image_name = self.history.pop()
            image = self.getImage(image_name)
            self.currentHistoryIndex = len(self.history) - 1
        return (image_name, image)

    def nextImage(self, skiped=False):
        image_name = self.server.nextImage(skiped)
        image = None
        if image_name:
            image = self.getImage(image_name)
        return (image_name, image)

    def getImage(self, name):
        return  open(name, 'rb', ).read()

    def imageTag(self, label):

        if self.currentImage:
            self.server.appendLabel(self.currentImage, label, self.username)
            if len(self.history) > self.historySize:
                self.history.pop(0)
            self.history.append(self.currentImage)
            self.currentImage = None
            # self.server.saveCSV()++
            # self.sendNextImage(data['skiped'])

    def sendPrevImage(self, data):
        if self.currentImage:

            if self.current_pool != self.HISTORY_POOL:
                self.server.returnImage(self.currentImage)

        self.currentImage, image = self.prevImage()
        self.sendImage(image)
        self.current_pool = self.HISTORY_POOL

    def sendNextImage(self, skiped):
        if self.currentImage:

            if self.current_pool != self.HISTORY_POOL:
                self.server.returnImage(self.currentImage, self.last_pool_is_skip)
            self.currentImage = None
        self.last_pool_is_skip = skiped
        self.current_pool = self.SKIPED_POOL if skiped else self.MAIN_POOL
        self.currentImage, image = self.nextImage(skiped)
        self.sendImage(image)

    def sendImage(self, image):
        self.emit('image', image)

    def onDisconnect(self):
        if self.username:
            print('User %s disconnected' % self.username)
        if self.currentImage:
            self.server.returnImage(self.currentImage)

    def deserialize_data(self, data):
        return pickle.loads(data)

    def serialize_data(self, data):
        return pickle.dumps(data)